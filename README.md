## qssi-user 11 RP1A.201005.001 2106020006 release-keys
- Manufacturer: oneplus
- Platform: kona
- Codename: OnePlus8T
- Brand: OnePlus
- Flavor: qssi-user
- Release Version: 11
- Id: RP1A.201005.001
- Incremental: 2106020006
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: OnePlus/OnePlus8T_IND/OnePlus8T:11/RP1A.201005.001/2106020006:user/release-keys
- OTA version: 
- Branch: qssi-user-11-RP1A.201005.001-2106020006-release-keys
- Repo: oneplus_oneplus8t_dump_12599


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
